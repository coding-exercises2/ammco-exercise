/*
person = {
  ...
  name = 'first_name last_name',
  friends = [ { friend1 }, { friend2 }, ...],
  favMovies = [ movie1, movie2, ...]
}
*/

function findMostPopularMovie(person) {
  let visited = new Set(),
    stack = [person],
    movieCount = {};

  let maxCount = 0,
    result = 'N/A';

  while (stack.length) {
    const currPerson = stack.pop();
    
    if (!visited.has(currPerson)) { // if current person has not been visited...
      visited.add(currPerson); // add current person to visited set
      const {favMovies, friends} = currPerson;
      
      for (const movie of favMovies) {
        movieCount[movie] = movieCount[movie] + 1 || 1; // increment count of movie by 1, initialize if movie does not exist
        if (movieCount[movie] > maxCount) {
          result = movie; // update most popular movie if count exceeds maxCount
          maxCount = movieCount[movie] // update maxCount
        }
      }
      
      stack.push(...friends); 
    } 
  }
  return result;
}

module.exports = findMostPopularMovie;
