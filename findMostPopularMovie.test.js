const findMostPopularMovie = require('./findMostPopularMovie');

let testPerson1 = {
    name: 'Test Dummy1',
    favMovies: [
        'The Shawshank Redemption',
        'The Godfather',
        '12 Angry Men',
    ], 
},
    testPerson2 = {
        name: 'Test Dummy2',
        favMovies: [
            'The Shawshank Redemption',
            'The Dark Knight',
            '12 Angry Men',
        ],
    },
    testPerson3 = {
        name: 'Test Dummy2',
        favMovies: [
            'The Shawshank Redemption',
            'Forrest Gump',
            'Fight Club',
        ],
    }

testPerson1.friends = [testPerson2];
testPerson2.friends = [testPerson1, testPerson3];
testPerson3.friends = [testPerson2];

test('finds Shawshank Redemption as the most popular movie', () => {
    expect(findMostPopularMovie(testPerson1)).toBe('The Shawshank Redemption');
});
